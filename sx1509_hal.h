/**
    @file sx1509_hal.h
    implementation sx1509 driver
    
    @author theadib@gmail.com, 2019
    
    SPDX-License-Identifier: MIT
*/

#ifndef INC_SX1509_HAL_H
#define INC_SX1509_HAL_H

#if defined(NRF52)

#include "nrf_twi_mngr.h"

/**
    the implementation is based on twi manager
    this twi_manager must have higher priority than the interrurpt you call the function from
    otherwise the TWI events are not handled
*/ 
static nrf_twi_mngr_t const *sx1509_nrf_twi_mngr;

static void sx1509_hal_init(void *hal)
{
	sx1509_nrf_twi_mngr = hal;
}

static unsigned int sx1509_hal_read(unsigned int addr)
{
     uint8_t addr_buffer[1];
     uint8_t data_buffer[1]; 
     nrf_twi_mngr_transfer_t transfers[2] =
    {
        NRF_TWI_MNGR_WRITE(SX1509_ADDRESS, addr_buffer, sizeof(addr_buffer), NRF_TWI_MNGR_NO_STOP),
        NRF_TWI_MNGR_READ(SX1509_ADDRESS, data_buffer, sizeof(data_buffer), 0)
    };
    
    addr_buffer[0] =  addr; 
    ret_code_t ret = nrf_twi_mngr_perform(sx1509_nrf_twi_mngr, NULL, transfers, 2, NULL);
	return data_buffer[0];
}

static unsigned int sx1509_hal_read_word(unsigned int addr)
{
    uint8_t addr_buffer[1];
    uint8_t data_buffer[2]; 
    nrf_twi_mngr_transfer_t 
    {
        NRF_TWI_MNGR_WRITE(SX1509_ADDRESS, addr_buffer, sizeof(addr_buffer), NRF_TWI_MNGR_NO_STOP),
        NRF_TWI_MNGR_READ(SX1509_ADDRESS, data_buffer, sizeof(data_buffer), 0)
    };
    addr_buffer[0] =  addr; 
    ret_code_t ret = nrf_twi_mngr_perform(sx1509_nrf_twi_mngr, NULL, transfers, 2, NULL);
	return (unsigned int)data_buffer[0]<<8 | data_buffer[1];
}

static void sx1509_hal_write_word(unsigned int addr, unsigned int val)
{
     uint8_t buffer[3]; 
     nrf_twi_mngr_transfer_t transfers[1] =
    {
        NRF_TWI_MNGR_WRITE(SX1509_ADDRESS, buffer, sizeof(buffer), 0),
    };

    buffer[0] = addr;
    buffer[1] = val >> 8;
    buffer[2] = val; 
    ret_code_t ret = nrf_twi_mngr_perform(sx1509_nrf_twi_mngr, NULL, transfers, 1, NULL);

}

#elif defined(STM32F407xx)

#include "stm32f4xx_hal.h"
static I2C_HandleTypeDef *sx1509_hi2c;

static void sx1509_hal_init(void *hal)
{
    sx1509_hi2c = hal;
}

/// read 8bit register
unsigned int sx1509_hal_read(unsigned int addr)
{
	uint8_t data[1];

	volatile HAL_StatusTypeDef status = HAL_I2C_Mem_Read(sx1509_hi2c, SX1509_ADDRESS<<1, addr, 1, data, 1, 100);
	return data[0];
}

/// read 2 8bit registers
unsigned int sx1509_hal_read_word(unsigned int addr)
{
	uint8_t data[2];

	volatile HAL_StatusTypeDef status = HAL_I2C_Mem_Read(sx1509_hi2c, SX1509_ADDRESS<<1, addr, 1, data, 2, 100);

	return ((unsigned int)data[0]<<8) | data[1];
}

/// write 2 8bit registers
void sx1509_hal_write_word(unsigned int addr, unsigned int val)
{
	uint8_t data[2] = { val >> 8, val };

	volatile HAL_StatusTypeDef status = HAL_I2C_Mem_Write(sx1509_hi2c, SX1509_ADDRESS<<1, addr, 1, data, 2, 100);
}

#else
#error UNKNOWN ARCHITECTURE
#endif

#endif // INC_SX1509_HAL_H
