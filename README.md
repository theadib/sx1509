# sx1509

semtech sx1509 gpio extender
driver implementation in C

only needed basic functionality implemented

## example implementation

based on sparkfun breakoutboard

the examples are implemented for
- STM32E407 based on Olimex STM32-E407 board
- NRF52832 based on nordic PCA10040 NRF52 DK board
