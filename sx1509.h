/**
    @file sx1509.h
    @author theadib@gmail.com, 2019
    
    SPDX-License-Identifier: MIT
*/

#ifndef INC_SX1509_H
#define INC_SX1509_H

extern unsigned int sx1509_init(void *);
extern void sx1509_configure_output(unsigned int);
extern void sx1509_set_pin(unsigned int, unsigned int);


#endif /* INC_SX1509_H */
