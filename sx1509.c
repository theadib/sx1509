/**
    @file sx1509.c
    implementation sx1509 driver
    
    @author theadib@gmail.com, 2019
    
    SPDX-License-Identifier: MIT
*/

#include <stdint.h>
#include "sx1509.h"
#include "SEGGER_RTT.h"


#define SX1509_ADDRESS 0x3E  // SX1509 I2C address

#define REG_TON_15 0x64	// default value 00h
#define REG_ION_15 0x65	// default value ffh

#define REG_DIR_B 0x0E
#define REG_DIR_A 0x0F
#define REG_DATA_B 0x10
#define REG_DATA_A 0x11

// hal include at this place, bcs defines must be propagated
#include "sx1509_hal.h"

unsigned int sx1509_init(void *hal)
{
    sx1509_hal_init(hal);
    
	unsigned int test = sx1509_hal_read(REG_TON_15);
    SEGGER_RTT_printf(0, "\n\nsx1509_init:%02X.", (int)test);
	if(test != 0x00) {
		return 0;
	}
	test = sx1509_hal_read(REG_ION_15);
    SEGGER_RTT_printf(0, "%02X.", (int)test);
	if(test != 0xff) {
		return 0;
	}
	return 1;
}

void sx1509_configure_output(unsigned int pin)
{
	unsigned int dir = sx1509_hal_read_word(REG_DIR_B);
	dir &= ~(1 << pin);	// clear direction bit for output
	sx1509_hal_write_word(REG_DIR_B, dir);
}

void sx1509_set_pin(unsigned int pin, unsigned int value)
{
    // read, modify, write sequence
	unsigned int data = sx1509_hal_read_word(REG_DATA_B);
    SEGGER_RTT_printf(0, "\nsx1509_set_pin:%04x. ", (int)data);
	if(value != 0) {
        // set bit position
		data |= (1 << pin);
	} else {
        // clear bit position
		data &= ~(1 << pin);
	}
    SEGGER_RTT_printf(0, "%04x. ", (int)data);
	sx1509_hal_write_word(REG_DATA_B, data);
}
